/**
  Andrea Rossolini Mat: 794193
  Luca Valentini Mat: 793819
  Filippo Orazi Mat: 801069
*/
#include "pong.h"
#define FLASH_LED 9
#define BUTTON_PIN 8
#define POTENTIOMETER A0
/** led's positions **/
#define LEFT_LED 4
#define CENTRAL_LED 5
#define RIGHT_LED 6
/** "BORDERS" of the game **/
#define MAX_VALUE 7
#define MIN_VALUE 3
/** BUTTON_RIGHT = PLAYER 2, BUTTON_LEFT = PLAYER 1 **/
#define BUTTON_LEFT 2
#define BUTTON_RIGHT 3

volatile bool gameStarted;     // true if the game is started, false instead
volatile int currentPosition;  // current position of the HIGH led
volatile char dir;             // 'l' -> LEFT_LED, 'r' -> RIGHT_LED
volatile int shot;             // number of shots
volatile int reactTime;        // time that the player have to react
int brightness;                // brightness of "FLASH_LED"
int fadeAmount;                // fading value of "FLASH_LED"
int ledSpeed;                  // led's speed
int delayTime;                 // time to wait before the loop restart (if the game is began)

void setup() {
  static int alreadyBegin=0;
  if(!alreadyBegin) {
    Serial.begin(9600);
    alreadyBegin=1;
  }
  Serial.println("Welcome to led pong: press key T3 to start");
  pinMode(BUTTON_PIN, INPUT);
  pinMode(FLASH_LED, OUTPUT);
  pinMode(POTENTIOMETER, INPUT);

  pinMode(LEFT_LED, OUTPUT);
  pinMode(CENTRAL_LED, OUTPUT);
  pinMode(RIGHT_LED, OUTPUT);

  pinMode(BUTTON_LEFT, INPUT);
  pinMode(BUTTON_RIGHT, INPUT);

  attachInterrupt(digitalPinToInterrupt(BUTTON_RIGHT), rightButtonPress, RISING);
  attachInterrupt(digitalPinToInterrupt(BUTTON_LEFT), leftButtonPress, RISING);
  gameStarted = false;
  brightness = 0;
  fadeAmount = 5;
  shot = 0;
  currentPosition = CENTRAL_LED;
  reactTime = 10000;
  delayTime = reactTime;
  dir = (random(0, 50)%2 == 0 ? 'r' : 'l');
}

void loop() {
  if (!gameStarted) {
    fadeFlashLed();
  } else {
    for (int i = 0; i < 100; i++)
      delayMicroseconds(delayTime);
    if (gameStarted)
      next();
  }
}

void next() {
  digitalWrite(currentPosition, LOW);
  dir == 'l' ? currentPosition-- : currentPosition++;
  if(currentPosition == MIN_VALUE || currentPosition == MAX_VALUE) {
    endGame(currentPosition == MIN_VALUE ? BUTTON_RIGHT : BUTTON_LEFT);
  } else {
    if(currentPosition != CENTRAL_LED) {
      delayTime=reactTime;
    }else{
      delayTime=ledSpeed;
    }
    digitalWrite(currentPosition, HIGH);
  }
}

void rightButtonPress() { // interrupt called by right button
  static unsigned long rightLastInterruptTime = 0;
  unsigned long interruptTime = millis();
  // if interrupts come faster than 200ms, assume it's a bounce and ignore it
  if (interruptTime - rightLastInterruptTime > 200) {
    if(gameStarted) {
      if(currentPosition == CENTRAL_LED || currentPosition == LEFT_LED) {
        endGame(BUTTON_LEFT);
      } else {
        changeDirection();
      }
    }
  }
  rightLastInterruptTime = interruptTime;
}

void leftButtonPress() { // interrupt called by left button
  static unsigned long leftLastInterruptTime = 0;
  unsigned long interruptTime = millis();
  // if interrupts come faster than 200ms, assume it's a bounce and ignore it
  if (interruptTime - leftLastInterruptTime > 200) {
    if(gameStarted){
      if(currentPosition == CENTRAL_LED || currentPosition == RIGHT_LED) {
        endGame(BUTTON_RIGHT);
      } else {
        changeDirection();
      }
    }
  }
  leftLastInterruptTime = interruptTime;
}

void changeDirection() {
  dir = (dir == 'l' ? 'r' : 'l');
  shot++;
  reactTime = reactTime - (reactTime / 8);
}

void endGame(int winnerButton) {
  digitalWrite(currentPosition, LOW);
  blinky(winnerButton == BUTTON_RIGHT ? RIGHT_LED : LEFT_LED);
  Serial.flush();
  Serial.print("Game over - The winner is player ");
  Serial.print(--winnerButton);
  Serial.print(" after ");
  Serial.print(shot);
  Serial.println(" shots.");
  setup();
}

void startGame(){
  Serial.println("Go");
  gameStarted = true;
  digitalWrite(CENTRAL_LED, HIGH);
  ledSpeed = 100000/map(analogRead(POTENTIOMETER),0,1023,1,5);
}

void blinky(int ledPin) {
  for(int i = 0; i < 4; i++) {
    digitalWrite(ledPin, digitalRead(ledPin)^1);
    for(int i = 0; i < 50; i++)
    delayMicroseconds(10000);
  }
}

void fadeFlashLed() {
  if (digitalRead(BUTTON_PIN) == HIGH) {
    analogWrite(FLASH_LED, 0);
    startGame();
  } else {
    analogWrite(FLASH_LED, brightness);
    brightness = brightness + fadeAmount;
    if (brightness <= 0 || brightness >= 255) {
      fadeAmount = -fadeAmount;
    }
    delay(20);
  }
}
