#ifndef __PONG__
#define __PONG__

/**********************************
N.B.
Le variabili "volatile" sono utilizzate soesso quando si fa uso di interrupt;
volatile infatti avverte il compilatore che quella variabile può cambiare spesso
ed in qualsiasi momento; quindi il compilatore deve ricaricare la variabile
ognivolta che se ne fa riferimento, piuttosto che fare affidamento su una copia
che potrebbe essere in un registro del processore.
************************************
Le variabili dichiarate STATICHE all'interno di una funzione servono in modo che
il loro valore viene memorizzato tra le chiamate di funzione e nessun'altra funzione
può cambiarne il valore
>https://www.arduino.cc/reference/en/language/variables/variable-scope--qualifiers/static/
************************************/
void next();
void rightButtonPress(); // interrupt called by right button
void leftButtonPress(); // interrupt called by left button
void changeDirection();
void endGame(int winnerButton);
void startGame();
void blinky(int ledPin);
void fadeFlashLed();

#endif
